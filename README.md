# [ExpressLRS Configurator][1]

The ExpressLRS Configurator, packaged as flatpak application.

[Get the latest build from `main`][2]


## Building

To build the application, you must have `podman` installed. Then, from this
directory, build the flatpak-builder container:

```bash
$ make container
```

And run the build process:

```bash
$ make
```

The Makefile will spawn the container for you. It will then download all
dependencies for building and in the end generate a `elrs-configurator.flatpak`
in the same directory it is run from.

Make sure you have the `flathub` remote installed and then execute:

```bash
$ flatpak --user install ./elrs-configurator.flatpak
# Or you try `make install`
```

> Note: If you installed flathub as system repository, replace `--user` with
> `--system`!

It will also install all the necessary runtimes for you. You can now start the
application either from the CLI:

```bash
$ flatpak run org.expresslrs.Configurator
```

or via your desktop launcher.


## Serial device access

TODO


[1]: https://github.com/ExpressLRS/ExpressLRS-Configurator
[2]: https://gitlab.com/hartan/elrs-configurator-flatpak/-/jobs/artifacts/main/raw/elrs-configurator.flatpak?job=flatpak

#!/usr/bin/env bash
ARCH=x86_64
VERSION=21.08
NODE=node16
BASE=org.freedesktop
APP_ID=org.expresslrs.Configurator
# Put a tag/branch/commit to build for here
ELRS_VERSION=v1.4.1

rm -rf build/
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub -y $BASE.Sdk/$ARCH/$VERSION $BASE.Platform/$ARCH/$VERSION org.electronjs.Electron2.BaseApp/$ARCH/$VERSION $BASE.Sdk.Extension.$NODE
flatpak build-init build "$APP_ID" $BASE.Sdk/$ARCH/$VERSION $BASE.Platform/$ARCH/$VERSION

# Clone ELRS-Configurator and generate offline yarn sources
if ! [[ -d ExpressLRS-Configurator ]]; then
    git clone https://github.com/ExpressLRS/ExpressLRS-Configurator.git
fi
pushd ExpressLRS-Configurator && git checkout $ELRS_VERSION && popd || exit 1
# See: https://github.com/flatpak/flatpak-builder-tools/tree/master/node
if ! [[ -d flatpak-builder-tools ]]; then
    git clone https://github.com/flatpak/flatpak-builder-tools.git
fi
if ! [[ -f generated-sources.json ]]; then
    python3 flatpak-builder-tools/node/flatpak-node-generator.py yarn ExpressLRS-Configurator/yarn.lock || exit 1
else
    echo "!!! SKIPPING YARN SOURCE GENERATION. CLEAN THIS PROJECT IF YOU WANT TO REGENERATE THESE. !!!"
    sleep 2
fi

flatpak-builder --force-clean --state-dir=build/flatpak-builder --repo=build/flatpak-repo build/flatpak-target "$APP_ID.yaml"
flatpak build-bundle build/flatpak-repo elrs-configurator.flatpak "$APP_ID"

.PHONY: clean mrproper container flatpak install

CONTAINER=localhost/flatpak-builder:latest
PODMAN_VOL_FLATPAK=flatpak-builder-var-lib-flatpak
VOL_FLATPAK=-v "$(PODMAN_VOL_FLATPAK):/var/lib/flatpak"
VOL_PROJECT=-v "$(shell pwd):/project" -w "/project"
VOL_DEVICES=-v "/dev:/dev" --security-opt label=disable
CAPS=--cap-add sys_admin,net_admin,net_raw

clean:
	! podman volume exists $(PODMAN_VOL_FLATPAK) || podman volume rm $(PODMAN_VOL_FLATPAK) >/dev/null
	rm -rf ./build logic.flatpak

mrproper: clean
	! podman image exists $(CONTAINER) || podman image rm $(CONTAINER) > /dev/null

container:
	podman build -t $(CONTAINER) -f Containerfile_build

.DEFAULT_GOAL :=
flatpak: container
	podman run --rm -i $(VOL_FLATPAK) $(VOL_PROJECT) $(VOL_DEVICES) $(CAPS) $(CONTAINER) ./buildit.sh

FLATHUB_REMOTE=$(shell flatpak remotes | grep "^flathub" | tr "[:space:]" " " | cut -d' ' -f2)
install: logic.flatpak
	flatpak --$(FLATHUB_REMOTE) install ./logic.flatpak
